<!----------------------------------------------------------------------------->

# cache (aws)

<!----------------------------------------------------------------------------->

## Description

Manage caching tools with [AWS] [ElastiCache] such as [Redis] and [Memcached].

<!----------------------------------------------------------------------------->

## Modules

* [elasticache/cluster/memcached](elasticache/cluster/memcached/README.md) - Manage [Memcached] [ElastiCache] clusters.
* [elasticache/cluster/redis](elasticache/cluster/redis/README.md) - Manage [Redis] [ElastiCache] clusters.
* [elasticache/subnet-group](elasticache/subnet-group/README.md) - Subnet group for use with [ElastiCache] clusters.

<!----------------------------------------------------------------------------->

[AWS]:         https://aws.amazon.com/
[Redis]:       https://redis.io/
[Memcached]:   https://memcached.org/
[ElastiCache]: https://aws.amazon.com/elasticache/

<!----------------------------------------------------------------------------->
