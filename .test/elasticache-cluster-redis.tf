################################################################################
# Modules
################################################################################

module "elasticache_cluster_redis" {
  source            = "../elasticache/cluster/redis"
  vpc               = local.vpc
  class             = local.class
  owner             = local.owner
  company           = local.company
  subnet_group_name = module.elasticache_subnet_group.name
}

################################################################################
