################################################################################
# Modules
################################################################################

module "elasticache_cluster_memcached" {
  source            = "../elasticache/cluster/memcached"
  vpc               = local.vpc
  class             = local.class
  owner             = local.owner
  company           = local.company
  subnet_group_name = module.elasticache_subnet_group.name
}

################################################################################
