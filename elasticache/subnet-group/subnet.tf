
################################################################################
# Optional Variables
################################################################################

variable "subnet_ids" {
  type        = list(string)
  default     = null
  description = "The IDs of the subnets to which this asset will span. Must be specified if subnets are created in the same Terraform run. Ignored if 'subnet_lookup' is 'true'."
}

variable "subnet_set" {
  type        = string
  default     = "default"
  description = "Which set of subnets to place this asset."
}

variable "subnet_tier" {
  type        = string
  default     = "backend"
  description = "Which tier of subnets to place this asset."
}

variable "subnet_zones" {
  type        = set(string)
  default     = null
  description = "Which availability zone(s) of subnets to place this asset. Specified as a single letter. If unspecified all are selected."
}

variable "subnet_lookup" {
  type        = bool
  default     = true
  description = "Lookup subnets based on 'subnet_set', 'subnet_tier' and 'subnet_zones'. Must be 'true' if 'subnet_ids' is not set."
}

################################################################################
# Locals
################################################################################

locals {
  subnet_ids   = var.subnet_lookup ? sort(tolist(data.aws_subnets.scope[0].ids)) : sort(var.subnet_ids)
  subnet_set   = var.subnet_lookup ? var.subnet_set : null
  subnet_tier  = var.subnet_lookup ? var.subnet_tier : null
  subnet_count = length(local.subnet_ids)
}

################################################################################
# Data Sources
################################################################################

data "aws_subnets" "scope" {
  count = var.subnet_lookup ? 1 : 0

  filter {
    name   = "vpc-id"
    values = toset([data.aws_vpc.scope.id])
  }

  filter {
    name   = "tag:Set"
    values = toset([local.subnet_set])
  }

  filter {
    name   = "tag:Tier"
    values = toset([local.subnet_tier])
  }

  dynamic "filter" {
    for_each = var.subnet_zones != null ? tolist([var.subnet_zones]) : []

    content {
      name   = "tag:Zone"
      values = filter.value
    }
  }
}

################################################################################
# Outputs
################################################################################

output "subnet_ids" {
  value = local.subnet_ids
}

output "subnet_set" {
  value = local.subnet_set
}

output "subnet_tier" {
  value = local.subnet_tier
}

output "subnet_count" {
  value = local.subnet_count
}

output "subnet_zones" {
  value = var.subnet_zones
}

output "subnet_lookup" {
  value = var.subnet_lookup
}

################################################################################
