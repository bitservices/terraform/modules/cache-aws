<!----------------------------------------------------------------------------->

# elasticache/subnet-group

#### A group of subnets that can be used by an [ElastiCache] instance

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/cache/aws//elasticache/subnet-group`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_subnet_group" {
  source  = "gitlab.com/bitservices/cache/aws//elasticache/subnet-group"
  vpc     = "sandpit01"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->

[ElastiCache]: https://aws.amazon.com/elasticache/

<!----------------------------------------------------------------------------->
