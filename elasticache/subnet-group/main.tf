################################################################################
# Required Variables
################################################################################

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "class" {
  type        = string
  default     = "default"
  description = "This forms the name of the ElastiCache subnet group. It is suffixed to 'vpc', 'subnet_tier' and 'subnet_set'."
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "The description of the ElastiCache subnet group."
}

################################################################################
# Locals
################################################################################

locals {
  name = format("%s-%s-%s-%s", var.vpc, var.subnet_tier, var.subnet_set, var.class)
}

################################################################################
# Resources
################################################################################

resource "aws_elasticache_subnet_group" "scope" {
  name        = local.name
  subnet_ids  = local.subnet_ids
  description = var.description

  tags = {
    Set     = var.subnet_set
    VPC     = var.vpc
    Name    = local.name
    Tier    = var.subnet_tier
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "class" {
  value = var.class
}

################################################################################

output "name" {
  value = aws_elasticache_subnet_group.scope.name
}

output "tags" {
  value = aws_elasticache_subnet_group.scope.tags_all
}

output "description" {
  value = aws_elasticache_subnet_group.scope.description
}

################################################################################
