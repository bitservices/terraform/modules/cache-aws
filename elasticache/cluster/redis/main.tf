################################################################################
# Required Variables
################################################################################

variable "class" {
  type        = string
  description = "This forms the name of the ElastiCache instance. It is used to seed a randomly generated ID prefixed with 'r-'."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "port" {
  type        = number
  default     = 6379
  description = "The port number on which each of the ElastiCache nodes will accept connections."
}

variable "type" {
  type        = string
  default     = "cache.t2.micro"
  description = "The instance type of the ElastiCache nodes."
}

variable "engine" {
  type        = string
  default     = "redis"
  description = "The Redis cache engine. Do not change this variable."
}

variable "shards" {
  type        = number
  default     = 1
  description = "The number of cache primary nodes (shards) that the ElastiCache cluster will have."
}

variable "sns_arn" {
  type        = string
  default     = null
  description = "An ARN of an SNS topic to send ElastiCache notifications to."
}

variable "replicas" {
  type        = number
  default     = 1
  description = "The number of replica nodes for each primary node (shard) that the ElastiCache cluster will have."
}

variable "parameters" {
  type        = string
  default     = "default.redis5.0.cluster.on"
  description = "Name of the ElastiCache parameter group to associate."
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "A user-created description for the ElastiCache cluster."
}

variable "auto_upgrade" {
  type        = bool
  default     = false
  description = "Specifies whether a minor engine upgrades will be applied automatically to the ElastiCache instances during the maintenance window."
}

variable "engine_version" {
  type        = string
  default     = "5.0.4"
  description = "The Redis cache engine version."
}

variable "apply_immediately" {
  type        = bool
  default     = true
  description = "Specifies whether any ElastiCache modifications are applied immediately, or during the next maintenance window."
}

variable "automatic_failover" {
  type        = bool
  default     = true
  description = "Enable automatic failover for ElastiCache cluster members. Must always be 'true'."
}

variable "maintenance_window" {
  type        = string
  default     = "mon:04:00-mon:05:00"
  description = "The window to perform maintenance in. Syntax: 'ddd:hh24:mi-ddd:hh24:mi'."
}

################################################################################

variable "backup_window" {
  type        = string
  default     = "02:00-03:00"
  description = "The daily time range (in UTC) during which automated backups are created if they are enabled. Must not overlap with 'maintenance_window'."
}

variable "backup_retention_days" {
  type        = number
  default     = 1
  description = "The amount of days to retain backups for."
}

################################################################################

variable "encrypt_at_rest" {
  type        = bool
  default     = true
  description = "Whether to enable encryption at rest for this ElastiCache cluster."
}

variable "encrypt_in_transit" {
  type        = bool
  default     = false
  description = "Whether to enable encryption in transit for this ElastiCache cluster. Redis does not support this natively and a TLS tunnel wrapper is required."
}

################################################################################

variable "subnet_set" {
  type        = string
  default     = "default"
  description = "Which set of subnets the subnet group that will be attached to this ElastiCache cluster belong too. Ignored if 'subnet_group_name' is set."
}

variable "subnet_tier" {
  type        = string
  default     = "backend"
  description = "Which tier of subnets the subnet group that will be attached to this ElastiCache cluster belong too. Ignored if 'subnet_group_name' is set."
}

variable "subnet_group_name" {
  type        = string
  default     = null
  description = "Full name of the subnet group to attach this ElastiCache cluster. Must be specified if subnet group is created in the same Terraform run."
}

variable "subnet_group_class" {
  type        = string
  default     = "default"
  description = "Identifier of the subnet group to attach this ElastiCache cluster to within 'subnet_set' and 'subnet_tier'. Ignored if 'subnet_group_name' is set."
}

################################################################################
# Locals
################################################################################

locals {
  subnet_group_name     = coalesce(var.subnet_group_name, format("%s-%s-%s-%s", var.vpc, var.subnet_tier, var.subnet_set, var.subnet_group_class))
  backup_retention_days = replace(split(".", var.type)[1], "/t\\d+/", "") != "" ? var.backup_retention_days : null
}

################################################################################
# Resources
################################################################################

resource "random_id" "cluster_id" {
  prefix      = "r-"
  byte_length = 9

  keepers = {
    "class" = var.class
    "vpc"   = var.vpc
  }
}

################################################################################

resource "aws_elasticache_replication_group" "scope" {
  port                       = var.port
  engine                     = var.engine
  node_type                  = var.type
  description                = var.description
  engine_version             = var.engine_version
  num_node_groups            = var.shards
  snapshot_window            = var.backup_window
  apply_immediately          = var.apply_immediately
  subnet_group_name          = local.subnet_group_name
  maintenance_window         = var.maintenance_window
  security_group_ids         = concat(var.security_group_ids, data.aws_security_group.scope.*.id)
  parameter_group_name       = var.parameters
  replication_group_id       = random_id.cluster_id.hex
  notification_topic_arn     = var.sns_arn
  replicas_per_node_group    = var.replicas
  snapshot_retention_limit   = local.backup_retention_days
  auto_minor_version_upgrade = var.auto_upgrade
  at_rest_encryption_enabled = var.encrypt_at_rest
  automatic_failover_enabled = var.automatic_failover
  transit_encryption_enabled = var.encrypt_in_transit

  tags = {
    VPC      = var.vpc
    Name     = random_id.cluster_id.hex
    Class    = var.class
    Owner    = var.owner
    Engine   = var.engine
    Region   = data.aws_region.scope.name
    Shards   = var.shards
    Company  = var.company
    Version  = var.engine_version
    Replicas = var.replicas
  }
}

################################################################################
# Outputs
################################################################################

output "class" {
  value = var.class
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "shards" {
  value = var.shards
}

output "replicas" {
  value = var.replicas
}

################################################################################

output "subnet_set" {
  value = var.subnet_set
}

output "subnet_tier" {
  value = var.subnet_tier
}

output "subnet_group_class" {
  value = var.subnet_group_class
}

################################################################################

output "id" {
  value = aws_elasticache_replication_group.scope.id
}

output "port" {
  value = aws_elasticache_replication_group.scope.port
}

output "type" {
  value = aws_elasticache_replication_group.scope.node_type
}

output "engine" {
  value = aws_elasticache_replication_group.scope.engine
}

output "address" {
  value = aws_elasticache_replication_group.scope.configuration_endpoint_address
}

output "members" {
  value = aws_elasticache_replication_group.scope.member_clusters
}

output "sns_arn" {
  value = aws_elasticache_replication_group.scope.notification_topic_arn
}

output "parameters" {
  value = aws_elasticache_replication_group.scope.parameter_group_name
}

output "description" {
  value = aws_elasticache_replication_group.scope.description
}

output "auto_upgrade" {
  value = aws_elasticache_replication_group.scope.auto_minor_version_upgrade
}

output "backup_window" {
  value = aws_elasticache_replication_group.scope.snapshot_window
}

output "engine_version" {
  value = aws_elasticache_replication_group.scope.engine_version
}

output "encrypt_at_rest" {
  value = aws_elasticache_replication_group.scope.at_rest_encryption_enabled
}

output "apply_immediately" {
  value = aws_elasticache_replication_group.scope.apply_immediately
}

output "subnet_group_name" {
  value = aws_elasticache_replication_group.scope.subnet_group_name
}

output "automatic_failover" {
  value = aws_elasticache_replication_group.scope.automatic_failover_enabled
}

output "encrypt_in_transit" {
  value = aws_elasticache_replication_group.scope.transit_encryption_enabled
}

output "maintenance_window" {
  value = aws_elasticache_replication_group.scope.maintenance_window
}

output "backup_retention_days" {
  value = aws_elasticache_replication_group.scope.snapshot_retention_limit
}

################################################################################
