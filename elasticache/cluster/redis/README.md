<!----------------------------------------------------------------------------->

# elasticache/cluster/redis

#### Manage [Redis] [ElastiCache] clusters

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/cache/aws//elasticache/cluster/redis`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "vpc"     { default = "sandpit01"                }
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_subnet_group" {
  source  = "gitlab.com/bitservices/cache/aws//elasticache/subnet-group"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
}

module "my_redis_cluster" {
  source            = "gitlab.com/bitservices/cache/aws//elasticache/cluster/redis"
  vpc               = var.vpc
  class             = "cache"
  owner             = var.owner
  company           = var.company
  subnet_group_name = module.my_subnet_group.name
}
```

<!----------------------------------------------------------------------------->

[Redis]:       https://redis.io/
[ElastiCache]: https://aws.amazon.com/elasticache/

<!----------------------------------------------------------------------------->
