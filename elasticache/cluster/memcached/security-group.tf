################################################################################
# Optional Variables
################################################################################

variable "security_group_ids" {
  type        = list(string)
  default     = []
  description = "List of security group IDs to attach to this ElastiCache instance."
}

variable "security_group_names" {
  type        = list(string)
  default     = []
  description = "List of security group names to attach to this ElastiCache instance. These are resolved and combined with 'security_group_ids'."
}

variable "security_group_vpc_prefix" {
  type        = bool
  default     = false
  description = "Should the name of the VPC be automatically prefixed to each security group in 'security_group_names'."
}

################################################################################
# Locals
################################################################################

locals {
  security_group_vpc_prefix = var.security_group_vpc_prefix ? format("%s-", var.vpc) : ""
}

################################################################################
# Data Sources
################################################################################

data "aws_security_group" "scope" {
  count  = length(var.security_group_names)
  name   = format("%s%s", local.security_group_vpc_prefix, var.security_group_names[count.index])
  vpc_id = data.aws_vpc.scope.id
}

################################################################################
# Outputs
################################################################################

output "security_group_count" {
  value = length(var.security_group_names)
}

output "security_group_vpc_prefix" {
  value = var.security_group_vpc_prefix
}

################################################################################

output "security_group_ids" {
  value = concat(var.security_group_ids, data.aws_security_group.scope.*.id)
}

################################################################################
