################################################################################
# Required Variables
################################################################################

variable "class" {
  type        = string
  description = "This forms the name of the ElastiCache instance. It is used to seed a randomly generated ID prefixed with 'm-'."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "port" {
  type        = number
  default     = 11211
  description = "The port number on which each of the ElastiCache nodes will accept connections."
}

variable "type" {
  type        = string
  default     = "cache.t2.micro"
  description = "The instance type of the ElastiCache nodes."
}

variable "zone" {
  type        = string
  default     = "*"
  description = "The availability zone of this ElastiCache cluster. '*' will spread across if 'nodes' is greater than '1'."
}

variable "nodes" {
  type        = number
  default     = 1
  description = "The initial number of cache nodes that the ElastiCache cluster will have."
}

variable "engine" {
  type        = string
  default     = "memcached"
  description = "The Memcached cache engine. Do not change this variable."
}

variable "sns_arn" {
  type        = string
  default     = null
  description = "An ARN of an SNS topic to send ElastiCache notifications to."
}

variable "parameters" {
  type        = string
  default     = "default.memcached1.5"
  description = "Name of the ElastiCache parameter group to associate."
}

variable "engine_version" {
  type        = string
  default     = "1.5.10"
  description = "The Memcached cache engine version."
}

variable "apply_immediately" {
  type        = bool
  default     = true
  description = "Specifies whether any ElastiCache modifications are applied immediately, or during the next maintenance window."
}

variable "maintenance_window" {
  type        = string
  default     = "mon:04:00-mon:05:00"
  description = "The window to perform maintenance in. Syntax: 'ddd:hh24:mi-ddd:hh24:mi'."
}

################################################################################

variable "subnet_set" {
  type        = string
  default     = "default"
  description = "Which set of subnets the subnet group that will be attached to this ElastiCache cluster belong too. Ignored if 'subnet_group_name' is set."
}

variable "subnet_tier" {
  type        = string
  default     = "backend"
  description = "Which tier of subnets the subnet group that will be attached to this ElastiCache cluster belong too. Ignored if 'subnet_group_name' is set."
}

variable "subnet_group_name" {
  type        = string
  default     = null
  description = "Full name of the subnet group to attach this ElastiCache cluster. Must be specified if subnet group is created in the same Terraform run."
}

variable "subnet_group_class" {
  type        = string
  default     = "default"
  description = "Identifier of the subnet group to attach this ElastiCache cluster to within 'subnet_set' and 'subnet_tier'. Ignored if 'subnet_group_name' is set."
}

################################################################################
# Locals
################################################################################

locals {
  zone              = local.multi_az ? "multi" : var.zone == "*" ? "single" : var.zone
  multi_az          = var.nodes > 1 && var.zone == "*" ? true : false
  subnet_group_name = coalesce(var.subnet_group_name, format("%s-%s-%s-%s", var.vpc, var.subnet_tier, var.subnet_set, var.subnet_group_class))
}

################################################################################
# Resources
################################################################################

resource "random_id" "cluster_id" {
  prefix      = "m-"
  byte_length = 9

  keepers = {
    "class" = var.class
    "vpc"   = var.vpc
  }
}

################################################################################

resource "aws_elasticache_cluster" "scope" {
  port                   = var.port
  engine                 = var.engine
  az_mode                = local.multi_az ? "cross-az" : "single-az"
  node_type              = var.type
  cluster_id             = random_id.cluster_id.hex
  engine_version         = var.engine_version
  num_cache_nodes        = var.nodes
  apply_immediately      = var.apply_immediately
  availability_zone      = var.zone != "*" ? format("%s%s", data.aws_region.scope.name, var.zone) : null
  subnet_group_name      = local.subnet_group_name
  maintenance_window     = var.maintenance_window
  security_group_ids     = concat(var.security_group_ids, data.aws_security_group.scope.*.id)
  parameter_group_name   = var.parameters
  notification_topic_arn = var.sns_arn

  tags = {
    VPC     = var.vpc
    Name    = random_id.cluster_id.hex
    Zone    = local.zone
    Class   = var.class
    Nodes   = var.nodes
    Owner   = var.owner
    Engine  = var.engine
    Region  = data.aws_region.scope.name
    Company = var.company
    Version = var.engine_version
  }
}

################################################################################
# Outputs
################################################################################

output "class" {
  value = var.class
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "zone" {
  value = local.zone
}

################################################################################

output "subnet_set" {
  value = var.subnet_set
}

output "subnet_tier" {
  value = var.subnet_tier
}

output "subnet_group_class" {
  value = var.subnet_group_class
}

################################################################################

output "multi_az" {
  value = local.multi_az
}

################################################################################

output "id" {
  value = aws_elasticache_cluster.scope.id
}

output "port" {
  value = aws_elasticache_cluster.scope.port
}

output "type" {
  value = aws_elasticache_cluster.scope.node_type
}

output "nodes" {
  value = aws_elasticache_cluster.scope.num_cache_nodes
}

output "engine" {
  value = aws_elasticache_cluster.scope.engine
}

output "address" {
  value = aws_elasticache_cluster.scope.cluster_address
}

output "sns_arn" {
  value = aws_elasticache_cluster.scope.notification_topic_arn
}

output "parameters" {
  value = aws_elasticache_cluster.scope.parameter_group_name
}

output "cache_nodes" {
  value = aws_elasticache_cluster.scope.cache_nodes
}

output "engine_version" {
  value = aws_elasticache_cluster.scope.engine_version
}

output "apply_immediately" {
  value = aws_elasticache_cluster.scope.apply_immediately
}

output "subnet_group_name" {
  value = aws_elasticache_cluster.scope.subnet_group_name
}

output "discovery_endpoint" {
  value = aws_elasticache_cluster.scope.configuration_endpoint
}

output "maintenance_window" {
  value = aws_elasticache_cluster.scope.maintenance_window
}

################################################################################
