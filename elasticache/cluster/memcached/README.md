<!----------------------------------------------------------------------------->

# elasticache/cluster/memcached

#### Manage [Memcached] [ElastiCache] clusters

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/cache/aws//elasticache/cluster/memcached`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "vpc"     { default = "sandpit01"                }
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_subnet_group" {
  source  = "gitlab.com/bitservices/cache/aws//elasticache/subnet-group"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
}

module "my_memcached_cluster" {
  source            = "gitlab.com/bitservices/cache/aws//elasticache/cluster/memcached"
  vpc               = module.my_vpc_core.name
  class             = "cache"
  owner             = var.owner
  company           = var.company
  subnet_group_name = module.my_subnet_group.name
}
```

<!----------------------------------------------------------------------------->

[Memcached]:   https://memcached.org/
[ElastiCache]: https://aws.amazon.com/elasticache/

<!----------------------------------------------------------------------------->
