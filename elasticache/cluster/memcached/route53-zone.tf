################################################################################
# Optional Variables
################################################################################

variable "route53_zone_base" {
  type        = string
  default     = "bitservices.io"
  description = "The DNS zone where Route53 records will be created for this ElastiCache instance."
}

variable "route53_zone_private" {
  type        = bool
  default     = true
  description = "Used with 'route53_zone_base' to find a private Hosted Zone."
}

variable "route53_zone_vpc_prefix" {
  type        = bool
  default     = true
  description = "Should the name of the VPC be automatically prefixed to 'route53_zone_base'."
}

################################################################################
# Locals
################################################################################

locals {
  route53_zone_name       = format("%s%s", local.route53_zone_vpc_prefix, var.route53_zone_base)
  route53_zone_vpc_prefix = var.route53_zone_vpc_prefix ? format("%s.", var.vpc) : ""
}

################################################################################
# Data Sources
################################################################################

data "aws_route53_zone" "scope" {
  count        = var.route53_records_create ? 1 : 0
  name         = local.route53_zone_name
  vpc_id       = var.route53_zone_private ? data.aws_vpc.scope.id : null
  private_zone = var.route53_zone_private
}

################################################################################
# Outputs
################################################################################

output "route53_zone_base" {
  value = var.route53_zone_base
}

output "route53_zone_vpc_prefix" {
  value = var.route53_zone_vpc_prefix
}

################################################################################

output "route53_zone_id" {
  value = length(data.aws_route53_zone.scope) == 1 ? data.aws_route53_zone.scope[0].id : null
}

output "route53_zone_name" {
  value = length(data.aws_route53_zone.scope) == 1 ? data.aws_route53_zone.scope[0].name : null
}

output "route53_zone_private" {
  value = length(data.aws_route53_zone.scope) == 1 ? data.aws_route53_zone.scope[0].private_zone : null
}

################################################################################
